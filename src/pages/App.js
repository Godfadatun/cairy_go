import React from "react";
import logo from "../statics/logo.svg";
import "../assets/css/App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          <code>Cairy Go</code>
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Air Dilevery
        </a>
      </header>
    </div>
  );
}

export default App;
